getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/eval3dp_data.ref','rb');
p1 = linspace(0, 2 * %pi, 10);
p2 = linspace(0, 2 * %pi, 10);
%ans = deff('[x,y,z]=scp(p1,p2)', ['x=p1.*sin(p1).*cos(p2)';'y=p1.*cos(p1).*cos(p2)';'z=p1.*sin(p2)']);
if load_ref('%ans') then   pause,end,

[Xf,Yf,Zf] = eval3dp(scp, p1, p2);
%ans = plot3d(Xf, Yf, Zf);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
