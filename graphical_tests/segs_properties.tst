getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/segs_properties_data.ref','rb');

%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,
//create a figure
a = get('current_axes');//get the handle of the newly created axes
a('data_bounds') = [-10,-10;10,10];
x = 2 * %pi * (0:7)/8;
xv = [2 * sin(x);9 * sin(x)];
yv = [2 * cos(x);9 * cos(x)];
%ans = xsegs(xv, yv, 1:8);
if load_ref('%ans') then   pause,end,


s = a('children');
if load_ref('s') then   pause,end,

s('arrow_size') = 1;
s('segs_color') = 15:22;
for j = 1:2,
  for i = 1:8,
    h = s.data(i * 2, j);
    s.data(i * 2, j) = s.data(i * 2 - 1, j);
    s.data(i * 2 - 1, j) = h;
  end,
end,

s('segs_color') = 5;//set all the colors to 5

s('clip_box') = [-4,4,8,8];
a('thickness') = 4;
xrect(s('clip_box'));

xdel_run(winsid());

mclose(%U);
