getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xpolys_data.ref','rb');
%ans = plot2d(0, 0, -1, '012', ' ', [0,0,1,1]);
if load_ref('%ans') then   pause,end,

%ans = rand('uniform');
if load_ref('%ans') then   pause,end,

%ans = xset('color', 3);
if load_ref('%ans') then   pause,end,

%ans = xpolys(rand(3, 5), rand(3, 5), [-1,-2,0,1,2]);
if load_ref('%ans') then   pause,end,

%ans = xset('default');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
