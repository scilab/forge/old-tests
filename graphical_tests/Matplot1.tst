getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/Matplot1_data.ref','rb');

//--- first example
set('figure_style', 'new');
clf_run();
ax = gca();//get current axes handle
ax('data_bounds') = [0,0;10,10];//set the data_bounds
ax('box') = 'on';//draw a box
a = 5 * ones(11, 11);a(2:10, 2:10) = 4;a(5:7, 5:7) = 2;
// first matrix in rectangle [1,1,3,3]
%ans = Matplot1(a, [1,1,3,3]);
if load_ref('%ans') then   pause,end,

a = ones(10, 10);a = 3 * tril(a) + 2 * a;
// second matrix in rectangle [5,6,7,8]
%ans = Matplot1(a, [5,6,7,8]);
if load_ref('%ans') then   pause,end,


//--- second example  (animation)
n = 100;

set('figure_style', 'new');
clf_run();
f = gcf();//get current figure handle
f('pixmap') = 'on';//double buffer mode
ax = gca();//get current axes handle
ax('data_bounds') = [0,0;10,10];//set the data_bounds
ax('box') = 'on';//draw a box
%ans = show_pixmap();
if load_ref('%ans') then   pause,end,

for k = -n:n,
  a = ones(n, n);
  a = 3 * tril(a, k) + 2 * a;
  a = a + a';
  k1 = 3 * (k + 100)/200;
  if k > (-n) then   %ans = delete(gce());
    if load_ref('%ans') then   pause,end,
  end,
  %ans = Matplot1(a, [k1,2,k1 + 7,9]);
  if load_ref('%ans') then   pause,end,

  %ans = show_pixmap();
  if load_ref('%ans') then   pause,end,
  //send double buffer to screen
end,


xdel_run(winsid());

mclose(%U);
