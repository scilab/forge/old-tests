getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/graphics_entities_data.ref','rb');


//Play this example line per line

%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,
//create a figure in entity mode

//get the handle on the Figure entity and display its properties
f = get('current_figure');
if load_ref('f') then   pause,end,

a = f('children');
if load_ref('a') then   pause,end,
// the handle on the Axes child
x = (1:10)';%ans = plot2d(x, [x.^2,x.^1.5]);
if load_ref('%ans') then   pause,end,

e = a('children');
if load_ref('e') then   pause,end,
//Compound of 2 polylines

p1 = e.children(1);
if load_ref('p1') then   pause,end,
//the last drawn polyline properties
p1('foreground') = 5;// change the polyline color
e.children.thickness = 5;// change the thickness of the two polylines

%ans = delete(e.children(2));
if load_ref('%ans') then   pause,end,


%ans = move(e('children'), [0,30]);
if load_ref('%ans') then   pause,end,
//translate the polyline

a('axes_bounds') = [0,0,0.5,0.5];

%ans = subplot(222);
if load_ref('%ans') then   pause,end,
//create a new Axes entity
plot(1:10);
a1 = f.children(1);//get its handle
copy(e('children'), a1);//copy the polyline of the first plot in the new Axes
a1('data_bounds') = [1,0;10,100];//change the Axes bounds

%ans = set('current_figure', 10);
if load_ref('%ans') then   pause,end,
//create a new figure with figure_id=10
%ans = plot3d();
if load_ref('%ans') then   pause,end,
//the drawing are sent to figure 10
%ans = set('current_figure', f);
if load_ref('%ans') then   pause,end,
//make the previous figure the current one
%ans = plot2d(x, x^3);
if load_ref('%ans') then   pause,end,
//the drawing are sent to the initial figure


xdel_run(winsid());

mclose(%U);
