getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/eval_cshep2d_data.ref','rb');
// see example section of cshep2d

// this example shows the behavior far from the interpolation points ...
%ans = deff('z=f(x,y)', 'z = 1+ 50*(x.*(1-x).*y.*(1-y)).^2');
if load_ref('%ans') then   pause,end,

x = linspace(0, 1, 10);
[X,Y] = ndgrid(x, x);
X = X(:);Y = Y(:);Z = f(X, Y);
S = cshep2d([X,Y,Z]);
// evaluation inside and outside the square [0,1]x[0,1]
m = 40;
xx = linspace(-1.5, 0.5, m);
[xp,yp] = ndgrid(xx, xx);
zp = eval_cshep2d(xp, yp, S);
// compute facet (to draw one color for extrapolation region
// and another one for the interpolation region)
[xf,yf,zf] = genfac3d(xx, xx, zp);
color = 2 * ones(1, size(zf, 2));
// indices corresponding to facet in the interpolation region
ind = find((((mean(xf, 'r') > 0) & (mean(xf, 'r') < 1)) & (mean(yf, 'r') > 0)) & (mean(yf, 'r') < 1));
color(ind) = 3;
xbasc_run();
%ans = plot3d(xf, yf, list(zf, color), flag=[2,6,4]);
if load_ref('%ans') then   pause,end,

%ans = legends(['extrapolation region','interpolation region'], [2,3], 1);
if load_ref('%ans') then   pause,end,

%ans = xselect();
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
