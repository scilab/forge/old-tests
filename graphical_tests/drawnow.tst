getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/drawnow_data.ref','rb');


%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,
// select entity based graphics
f = get('current_figure');
if load_ref('f') then   pause,end,
// handle of the current figure

%ans = drawlater();
if load_ref('%ans') then   pause,end,

subplot(221);
t = 1:10;%ans = plot2d(t, t.^2);
if load_ref('%ans') then   pause,end,

subplot(222);
x = 0:1:7;%ans = plot2d(x, cos(x), 2);
if load_ref('%ans') then   pause,end,

subplot(234);
plot2d(t, cos(t), 3);
subplot(235);
plot2d(x, sin(2 * x), 5);
subplot(236);
plot2d(t, tan(2 * t));

draw(gca());//draw the current axes and its children
draw(f.children([3,4]));// draw the specified axes and their children
drawnow();// draw the current figure


xdel_run(winsid());

mclose(%U);
