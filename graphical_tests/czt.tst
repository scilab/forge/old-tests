getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/czt_data.ref','rb');
a = 0.7 * exp(%i * %pi/6);
[ffr,bds] = xgetech();//preserve current context
rect = [-1.2,-1.2 * sqrt(2),1.2,1.2 * sqrt(2)];
t = 2 * %pi * (0:179)/179;xsetech([0,0,0.5,1]);
%ans = plot2d(sin(t)', cos(t)', 2, '012', ' ', rect);
if load_ref('%ans') then   pause,end,

%ans = plot2d([0,real(a)]', [0,imag(a)]', 3, '000');
if load_ref('%ans') then   pause,end,

%ans = xsegs([-1,0;1,0], [0,-1;0,1]);
if load_ref('%ans') then   pause,end,

w0 = 0.93 * exp(-%i * %pi/15);w = exp(-(0:9) * log(w0));z = a * w;
zr = real(z);zi = imag(z);
%ans = plot2d(zr', zi', 5, '000');
if load_ref('%ans') then   pause,end,

xsetech([0.5,0,0.5,1]);
%ans = plot2d(sin(t)', cos(t)', 2, '012', ' ', rect);
if load_ref('%ans') then   pause,end,

%ans = plot2d([0,real(a)]', [0,imag(a)]', -1, '000');
if load_ref('%ans') then   pause,end,

%ans = xsegs([-1,0;1,0], [0,-1;0,1]);
if load_ref('%ans') then   pause,end,

w0 = w0/(0.93 * 0.93);w = exp(-(0:9) * log(w0));z = a * w;
zr = real(z);zi = imag(z);
%ans = plot2d(zr', zi', 5, '000');
if load_ref('%ans') then   pause,end,

xsetech(ffr, bds);//restore context
xdel_run(winsid());

mclose(%U);
