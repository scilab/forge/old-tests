getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/gca_data.ref','rb');

%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,
//create a figure
%ans = subplot(211);
if load_ref('%ans') then   pause,end,

a = gca();
if load_ref('a') then   pause,end,
//get the current axes
a('box') = 'off';
t = -%pi:0.3:%pi;plot3d(t, t, sin(t)' * cos(t), 80, 50, 'X@Y@Z', [5,2,4]);
%ans = subplot(212);
if load_ref('%ans') then   pause,end,

plot2d();//simple plot
a = gca();
if load_ref('a') then   pause,end,
//get the current axes
a('box') = 'off';
a('x_location') = 'middle';
a.parent.background = 4;
%ans = delete(gca());
if load_ref('%ans') then   pause,end,
// delete the current axes
%ans = xdel_run(0);
if load_ref('%ans') then   pause,end,
//delete a graphics window

xdel_run(winsid());

mclose(%U);
