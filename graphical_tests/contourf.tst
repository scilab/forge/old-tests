getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/contourf_data.ref','rb');
%ans = contourf(1:10, 1:10, rand(10, 10), 5, 1:5, '011', ' ', [0,0,11,11]);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
