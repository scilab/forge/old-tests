getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/beta_data.ref','rb');
// example 1 :
%ans = beta(5, 2) - beta(2, 5);
if load_ref('%ans') then   pause,end,
// symetry (must be exactly 0)
%ans = beta(0.5, 0.5);
if load_ref('%ans') then   pause,end,
// exact value is pi

// example 2 : an error study based on the relation  B(1,x) = 1/x
// (computing 1/x must lead to only a relative error of eps_m, so
//  it may be used near as a reference to evaluate the error in B(1,x))
x = logspace(-8, 8, 20000)';
e = beta(ones(x), x) - 1 ./ x;
er = abs(e) .* x;
ind = find(er ~= 0);
eps = ones(x(ind)) * number_properties('eps');
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot2d(x(ind), [er(ind),eps,2 * eps], style=[1,2,3], logflag='ll', leg='er@eps_m@2 eps_m');
if load_ref('%ans') then   pause,end,

%ans = xtitle('approximate relative error in computing beta(1,x)');
if load_ref('%ans') then   pause,end,

%ans = xselect();
if load_ref('%ans') then   pause,end,


// example 3 : plotting the beta function
t = linspace(0.2, 10, 60);
X = t' * ones(t);Y = ones(t') * t;
Z = beta(X, Y);
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot3d(t, t, Z, flag=[2,4,4], leg='x@y@z', alpha=75, theta=30);
if load_ref('%ans') then   pause,end,

%ans = xtitle('The beta function on [0.2,10]x[0.2,10]');
if load_ref('%ans') then   pause,end,

%ans = xselect();
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
