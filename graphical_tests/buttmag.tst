getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/buttmag_data.ref','rb');
//squared magnitude response of Butterworth filter
h = buttmag(13, 300, 1:1000);
mag = 20 * log(h)'/log(10);
%ans = plot2d((1:1000)', mag, 2, '011', ' ', [0,-180,1000,20]);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
