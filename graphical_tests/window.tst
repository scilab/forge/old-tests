getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/window_data.ref','rb');
// Hamming window
%ans = clf_run();
if load_ref('%ans') then   pause,end,

N = 64;
w = window('hm', N);
subplot(121);%ans = plot2d(1:N, w, style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,

%ans = subplot(122);
if load_ref('%ans') then   pause,end,

n = 256;[W,fr] = frmag(w, n);
%ans = plot2d(fr, 20 * log10(W), style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,


//Kaiser window
%ans = clf_run();
if load_ref('%ans') then   pause,end,

N = 64;
w = window('kr', N, 6);
subplot(121);%ans = plot2d(1:N, w, style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,

%ans = subplot(122);
if load_ref('%ans') then   pause,end,

n = 256;[W,fr] = frmag(w, n);
%ans = plot2d(fr, 20 * log10(W), style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,


//Chebyshev window
%ans = clf_run();
if load_ref('%ans') then   pause,end,

N = 64;
[w,df] = window('ch', N, [0.005,-1]);
subplot(121);%ans = plot2d(1:N, w, style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,

%ans = subplot(122);
if load_ref('%ans') then   pause,end,

n = 256;[W,fr] = frmag(w, n);
%ans = plot2d(fr, 20 * log10(W), style=color('blue'));
if load_ref('%ans') then   pause,end,

%ans = set(gca(), 'grid', [1,1] * color('gray'));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
