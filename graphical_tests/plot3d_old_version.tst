getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/plot3d_old_version_data.ref','rb');

// simple plot using z=f(x,y)
t = (0:0.3:2 * %pi)';z = sin(t) * cos(t');
%ans = plot3d(t, t, z);
if load_ref('%ans') then   pause,end,

// same plot using facets computed by genfac3d
[xx,yy,zz] = genfac3d(t, t, z);
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot3d(xx, yy, zz);
if load_ref('%ans') then   pause,end,

// multiple plots
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot3d([xx,xx], [yy,yy], [zz,4 + zz]);
if load_ref('%ans') then   pause,end,

// multiple plots using colors
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot3d([xx,xx], [yy,yy], list([zz,zz + 4], [4 * ones(1, 400),5 * ones(1, 400)]));
if load_ref('%ans') then   pause,end,

// simple plot with viewpoint and captions
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot3d(1:10, 1:20, 10 * rand(10, 20), 35, 45, 'X@Y@Z', [2,2,3]);
if load_ref('%ans') then   pause,end,

// plot of a sphere using facets computed by eval3dp
deff('[x,y,z]=sph(alp,tet)', ['x=r*cos(alp).*cos(tet)+orig(1)*ones(tet)';'y=r*cos(alp).*sin(tet)+orig(2)*ones(tet)';'z=r*sin(alp)+orig(3)*ones(tet)']);
r = 1;orig = [0,0,0];
[xx,yy,zz] = eval3dp(sph, linspace(-%pi/2, %pi/2, 40), linspace(0, %pi * 2, 20));
xbasc_run();%ans = plot3d(xx, yy, zz);
if load_ref('%ans') then   pause,end,


xbasc_run();xset('colormap', hotcolormap(128));
r = 0.3;orig = [1.5,0,0];
[xx1,yy1,zz1] = eval3dp(sph, linspace(-%pi/2, %pi/2, 40), linspace(0, %pi * 2, 20));
cc = (xx + zz + 2) * 32;cc1 = (xx1 - orig(1) + zz1/r + 2) * 32;
xbasc_run();%ans = plot3d1([xx,xx1], [yy,yy1], list([zz,zz1], [cc,cc1]), 70, 80);
if load_ref('%ans') then   pause,end,


xbasc_run();%ans = plot3d1([xx,xx1], [yy,yy1], list([zz,zz1], [cc,cc1]), theta=70, alpha=80, flag=[5,6,3]);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
