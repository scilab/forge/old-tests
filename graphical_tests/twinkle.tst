getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/twinkle_data.ref','rb');
x = linspace(-2 * %pi, 2 * %pi, 100)';
plot2d(x, [sin(x),cos(x)]);
e = gce();p1 = e.children(1);p2 = e.children(2);
// cos plot twinkle
%ans = twinkle(p1);
if load_ref('%ans') then   pause,end,

// sin plot twinkle 10 times
%ans = twinkle(p2, 10);
if load_ref('%ans') then   pause,end,

// axes twinkle
%ans = twinkle(gca());
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
