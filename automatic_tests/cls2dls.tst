getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/cls2dls_data.ref','rb');
s = poly(0, 's');z = poly(0, 'z');
sl = syslin('c', (s + 1)/(s^2 - 5 * s + 2));//Continuous-time system in transfer form
slss = tf2ss(sl);//Now in state-space form
sl1 = cls2dls(slss, 0.2);//sl1= output of cls2dls
sl1t = ss2tf(sl1);
if load_ref('sl1t') then   pause,end,
// Converts in transfer form
sl2 = horner(sl, 2/0.2 * (z - 1)/(z + 1));
if load_ref('sl2') then   pause,end,
//Compare sl2 and sl1
xdel_run(winsid());

mclose(%U);
