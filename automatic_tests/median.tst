getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/median_data.ref','rb');

A = [1,2,10;7,7.1,7.01];
%ans = median(A);
if load_ref('%ans') then   pause,end,

%ans = median(A, 'r');
if load_ref('%ans') then   pause,end,

%ans = median(A, 'c');
if load_ref('%ans') then   pause,end,

A = matrix([-9,3,-8,6,74,39,12,-6,-89,23,65,34], [2,3,2]);
%ans = median(A, 3);
if load_ref('%ans') then   pause,end,

%ans = median(A, 'm');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
