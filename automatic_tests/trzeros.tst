getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/trzeros_data.ref','rb');
W1 = ssrand(2, 2, 5);%ans = trzeros(W1);
if load_ref('%ans') then   pause,end,
//call trzeros
%ans = roots(det(systmat(W1)));
if load_ref('%ans') then   pause,end,
//roots of det(system matrix)
s = poly(0, 's');W = [1/(s + 1);1/(s - 2)];W2 = (s - 3) * W * W';[nt,dt,rk] = trzeros(W2);
St = systmat(tf2ss(W2));[Q,Z,Qd,Zd,numbeps,numbeta] = kroneck(St);
St1 = Q * St * Z;rowf = Qd(1) + Qd(2) + 1:Qd(1) + Qd(2) + Qd(3);
colf = Zd(1) + Zd(2) + 1:Zd(1) + Zd(2) + Zd(3);
%ans = roots(St1(rowf, colf));
if load_ref('%ans') then   pause,end,
%ans = nt ./ dt;
if load_ref('%ans') then   pause,end,
//By Kronecker form
xdel_run(winsid());

mclose(%U);
