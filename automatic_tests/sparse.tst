getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/sparse_data.ref','rb');
sp = sparse([1,2;4,5;3,10], [1,2,3]);
if load_ref('sp') then   pause,end,

%ans = size(sp);
if load_ref('%ans') then   pause,end,

x = rand(2, 2);%ans = abs(x) - full(abs(sparse(x)));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
