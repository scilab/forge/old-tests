getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/sum_data.ref','rb');
A = [1,2;3,4];
%ans = trace(A) - sum(diag(A));
if load_ref('%ans') then   pause,end,

%ans = sum(A, 'c') - A * ones(2, 1);
if load_ref('%ans') then   pause,end,

%ans = sum(A + %i);
if load_ref('%ans') then   pause,end,

A = sparse(A);%ans = sum(A, 'c') - A * ones(2, 1);
if load_ref('%ans') then   pause,end,

s = poly(0, 's');
M = [s,%i + s;s^2,1];
%ans = sum(M);
if load_ref('%ans') then   pause,end,
%ans = sum(M, 2);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
