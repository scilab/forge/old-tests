getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/glever_data.ref','rb');
s = %s;F = [-1,s,0,0;0,-1,0,0;0,0,s - 2,0;0,0,0,s - 1];
[Bfs,Bis,chis] = glever(F);
if load_ref('chis') then   pause,end,
if load_ref('Bis') then   pause,end,
if load_ref('Bfs') then   pause,end,

%ans = inv(F) - (Bfs/chis - Bis);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
