getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/pencan_data.ref','rb');
F = randpencil([], [1,2], [1,2,3], []);
F = rand(6, 6) * F * rand(6, 6);
[Q,M,i1] = pencan(F);
W = clean(M * F * Q);
if load_ref('W') then   pause,end,

%ans = roots(det(W(1:i1, 1:i1)));
if load_ref('%ans') then   pause,end,

%ans = det(W($ - 2:$, $ - 2:$));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
