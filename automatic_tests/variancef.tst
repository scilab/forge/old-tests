getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/variancef_data.ref','rb');

x = [0.2113249,0.0002211,0.6653811;0.7560439,0.9546254,0.6283918];
if load_ref('x') then   pause,end,

fre = [1,2,3;3,4,3];
if load_ref('fre') then   pause,end,

m = variancef(x, fre);
if load_ref('m') then   pause,end,

m = variancef(x, fre, 'r');
if load_ref('m') then   pause,end,

m = variancef(x, fre, 'c');
if load_ref('m') then   pause,end,


xdel_run(winsid());

mclose(%U);
