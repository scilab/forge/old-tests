getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/profile_data.ref','rb');
//define function and prepare it for profiling
%ans = deff('x=foo(n)', ['if n==0 then';
  '  x=[]';
  'else';
  '  x=0';
  '  for k=1:n';
  '    s=svd(rand(n+10,n+10))';
  '    x=x+s(1)';
  '  end';
  'end'], 'p');
if load_ref('%ans') then   pause,end,

//call the function
%ans = foo(10);
if load_ref('%ans') then   pause,end,

//get execution profiles
%ans = profile(foo);
if load_ref_nocheck('%ans') then   pause,end,

//call the function
%ans = foo(20);
if load_ref('%ans') then   pause,end,

%ans = profile(foo);
if load_ref_nocheck('%ans') then   pause,end,
//execution profiles are cumulated
xdel_run(winsid());

mclose(%U);
