getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/str2code_data.ref','rb');
%ans = str2code('Scilab');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
