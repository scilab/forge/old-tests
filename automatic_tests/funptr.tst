getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/funptr_data.ref','rb');
// Suppose you want to load some codes via the dynamic
// loading facilities offers by addinter. By default
// arguments are passed by values but if you want to
// pass them by reference you can do the following
// (name being the scilab name of one of the interfaced
// routines) :
//
// addinter(files,spnames,fcts)  // args passed by values
// num_interface = floor(funptr(name)/100)
// intppty(num_interface)  // args now passed by reference
//
// Note that if you enter the following
//
// intppty()
//
// you will see all the interfaces working by reference
xdel_run(winsid());

mclose(%U);
