getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/datenum_data.ref','rb');
%ans = datenum();
if load_ref_nocheck('%ans') then   pause,end,


%ans = datenum(1973, 8, 4);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
