getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/spaninter_data.ref','rb');
A = rand(5, 3) * rand(3, 4);// A is 5 x 4, rank=3
B = [A(:, 2),rand(5, 1)] * rand(2, 2);
[X,dim] = spaninter(A, B);
X1 = X(:, 1:dim);//The intersection
%ans = svd(A);
if load_ref('%ans') then   pause,end,
%ans = svd([X1,A]);
if load_ref('%ans') then   pause,end,
// X1 in span(A)
%ans = svd(B);
if load_ref('%ans') then   pause,end,
%ans = svd([B,X1]);
if load_ref('%ans') then   pause,end,
// X1 in span(B)
xdel_run(winsid());

mclose(%U);
