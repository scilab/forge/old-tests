getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/setdiff_data.ref','rb');
a = [223;111;2;4;2;2];
b = [2;3;21;223;123;22];
%ans = setdiff(a, b);
if load_ref('%ans') then   pause,end,

[v,k] = setdiff(string(a), string(b));
if load_ref('k') then   pause,end,
if load_ref('v') then   pause,end,

xdel_run(winsid());

mclose(%U);
