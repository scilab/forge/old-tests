getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/cond_data.ref','rb');
A = testmatrix('hilb', 6);
%ans = cond(A);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
