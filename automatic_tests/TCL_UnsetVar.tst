getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/TCL_UnsetVar_data.ref','rb');
%ans = TCL_SetVar('Scilab', 'OK');
if load_ref('%ans') then   pause,end,

%ans = TCL_GetVar('Scilab');
if load_ref('%ans') then   pause,end,

%ans = TCL_UnsetVar('Scilab');
if load_ref('%ans') then   pause,end,

%ans = TCL_ExistVar('Scilab');
if load_ref('%ans') then   pause,end,


TCL_CreateSlave('InterpSlave');
%ans = TCL_SetVar('Scilab', 'Good', 'InterpSlave');
if load_ref('%ans') then   pause,end,

%ans = TCL_GetVar('Scilab', 'InterpSlave');
if load_ref('%ans') then   pause,end,

%ans = TCL_UnsetVar('Scilab', 'InterpSlave');
if load_ref('%ans') then   pause,end,

%ans = TCL_ExistVar('Scilab', 'InterpSlave');
if load_ref('%ans') then   pause,end,

%ans = TCL_DeleteInterp('InterpSlave');
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
