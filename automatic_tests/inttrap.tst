getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/inttrap_data.ref','rb');
t = 0:0.1:%pi;
if load_ref('t') then   pause,end,

%ans = inttrap(t, sin(t));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
