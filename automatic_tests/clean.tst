getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/clean_data.ref','rb');
x = poly(0, 'x');
w = [x,1,2 + x;3 + x,2 - x,x^2;1,2,3 + x]/3;
%ans = w * inv(w);
if load_ref('%ans') then   pause,end,

%ans = clean(w * inv(w));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
