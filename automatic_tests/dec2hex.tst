getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/dec2hex_data.ref','rb');
%ans = dec2hex([2748,10;11,3]);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
