getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/chepol_data.ref','rb');
%ans = chepol(4, 'x');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
