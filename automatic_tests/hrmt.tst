getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/hrmt_data.ref','rb');
x = poly(0, 'x');
v = [x * (x + 1),(x^2) * (x + 1),(x - 2) * (x + 1),(3 * (x^2) + 2) * (x + 1)];
[pg,U] = hrmt(v);U = clean(U);
if load_ref('U') then   pause,end,

%ans = det(U);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
