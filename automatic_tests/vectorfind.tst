getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/vectorfind_data.ref','rb');
alr = [1,2,2;
  1,2,1;
  1,1,2;
  1,1,1;
  1,2,1];
ind = vectorfind(alr, [1,2,1], 'r');
if load_ref('ind') then   pause,end,

ind = vectorfind(string(alr), string([1,2,1]), 'r');
if load_ref('ind') then   pause,end,

xdel_run(winsid());

mclose(%U);
