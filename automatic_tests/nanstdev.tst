getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/nanstdev_data.ref','rb');

x = [0.2113249,0.0002211,0.6653811;
  0.7560439,%nan,0.6283918;
  0.3,0.2,0.5];
s = nanstdev(x);
if load_ref('s') then   pause,end,

s = nanstdev(x, 'r');
if load_ref('s') then   pause,end,

s = nanstdev(x, 'c');
if load_ref('s') then   pause,end,

xdel_run(winsid());

mclose(%U);
