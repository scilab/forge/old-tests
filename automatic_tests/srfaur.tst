getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/srfaur_data.ref','rb');
//GENERATE SIGNAL
x = %pi/10:%pi/10:102.4 * %pi;
rand('seed', 0);rand('normal');
y = [1;1] * sin(x) + [sin(2 * x);sin(1.9 * x)] + rand(2, 1024);
//COMPUTE CORRELATIONS
c = [];for j = 1:2,  for k = 1:2,  c = [c;corr(y(k, :), y(j, :), 64)];end,end,
c = matrix(c, 2, 128);
//FINDING H,F,G with 6 states
hk = hank(20, 20, c);
[H,F,G] = phc(hk, 2, 6);
//SOLVING RICCATI EQN
r0 = c(1:2, 1:2);
[P,s,t,l,Rt,Tt] = srfaur(H, F, G, r0, 200);
//Make covariance matrix exactly symetric
Rt = (Rt + Rt')/2;
if load_ref('Rt') then   pause,end,

xdel_run(winsid());

mclose(%U);
