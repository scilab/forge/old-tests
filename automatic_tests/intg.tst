getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/intg_data.ref','rb');
//Scilab function case
function y=f(x)
  ,y=x*sin(30*x)/sqrt(1-((x/(2*%pi))^2)),
endfunction
if load_ref('%ans') then   pause,end,

exact = -2.5432596188;
I = intg(0, 2 * %pi, f);
if load_ref('I') then   pause,end,

%ans = abs(exact - I);
if load_ref('%ans') then   pause,end,


//Scilab function case with parameter
function y=f1(x,w)
  ,y=x*sin(w*x)/sqrt(1-((x/(2*%pi))^2)),
endfunction
if load_ref('%ans') then   pause,end,

I = intg(0, 2 * %pi, list(f1, 30));
if load_ref('I') then   pause,end,

%ans = abs(exact - I);
if load_ref('%ans') then   pause,end,



// Fortran code case (Fortran compiler required)
// write down the fortran code
F = ['      double precision function ffun(x)';
  '      double precision x,pi';
  '      pi=3.14159265358979312d+0';
  '      ffun=x*sin(30.0d+0*x)/sqrt(1.0d+0-(x/(2.0d+0*pi))**2)';
  '      return';
  '      end'];
%ans = mputl(F, TMPDIR + '/ffun.f');
if load_ref('%ans') then   pause,end,

// compile the fortran code
l = ilib_for_link('ffun', 'ffun.o', [], 'f', TMPDIR + '/Makefile');
// incremental linking
%ans = link(l, 'ffun', 'f');

if load_ref_nocheck('%ans') then   pause,end,

//integrate the function
I = intg(0, 2 * %pi, 'ffun');
if load_ref('I') then   pause,end,

%ans = abs(exact - I);
if load_ref('%ans') then   pause,end,


// C code case (C compiler required)
// write down the C code
C = ['#include <math.h>';
  'double cfun(double *x)';
  '{';
  '  double y,pi=3.14159265358979312;';
  '  y=*x/(2.0e0*pi);';
  '  return *x*sin(30.0e0**x)/sqrt(1.0e0-y*y);';
  '}'];
%ans = mputl(C, TMPDIR + '/cfun.c');
if load_ref('%ans') then   pause,end,

// compile the C code
l = ilib_for_link('cfun', 'cfun.o', [], 'c', TMPDIR + '/Makefile');
// incremental linking
%ans = link(l, 'cfun', 'c');
if load_ref_nocheck('%ans') then   pause,end,

//integrate the function
I = intg(0, 2 * %pi, 'cfun');
if load_ref('I') then   pause,end,

%ans = abs(exact - I);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
