getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/isnan_data.ref','rb');
%ans = isnan([1,0.01,-%nan,%inf - %inf]);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
