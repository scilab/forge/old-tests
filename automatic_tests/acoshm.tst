getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/acoshm_data.ref','rb');
A = [1,2;3,4];
%ans = coshm(acoshm(A));
if load_ref('%ans') then   pause,end,

A(1, 1) = A(1, 1) + %i;
%ans = coshm(acoshm(A));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
