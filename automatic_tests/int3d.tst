getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/int3d_data.ref','rb');
X = [0;1;0;0];
Y = [0;0;1;0];
Z = [0;0;0;1];
[RESULT,ERROR] = int3d(X, Y, Z, 'int3dex');
if load_ref('ERROR') then   pause,end,
if load_ref('RESULT') then   pause,end,

// computes the integrand exp(x*x+y*y+z*z) over the
//tetrahedron (0.,0.,0.),(1.,0.,0.),(0.,1.,0.),(0.,0.,1.)


//integration over a cube  -1<=x<=1;-1<=y<=1;-1<=z<=1

//  bottom  -top-     right    -left-   front   -rear-
X = [0,0,0,0,0,0,0,0,0,0,0,0;
  -1,-1,-1,-1,1,1,-1,-1,-1,-1,-1,-1;
  1,-1,1,-1,1,1,-1,-1,1,-1,1,-1;
  1,1,1,1,1,1,-1,-1,1,1,1,1];
Y = [0,0,0,0,0,0,0,0,0,0,0,0;
  -1,-1,-1,-1,-1,1,-1,1,-1,-1,1,1;
  -1,1,-1,1,1,1,1,1,-1,-1,1,1;
  1,1,1,1,-1,-1,-1,-1,-1,-1,1,1];
Z = [0,0,0,0,0,0,0,0,0,0,0,0;
  -1,-1,1,1,-1,1,-1,1,-1,-1,-1,-1;
  -1,-1,1,1,-1,-1,-1,-1,-1,1,-1,1;
  -1,-1,1,1,1,-1,1,-1,1,1,1,1];

function v=f(xyz,numfun)
  ,v=exp(xyz'*xyz),
endfunction
if load_ref('%ans') then   pause,end,

[result,err] = int3d(X, Y, Z, f, 1, [0,100000,0.00001,0.0000001]);
if load_ref('err') then   pause,end,
if load_ref('result') then   pause,end,


function v=f(xyz,numfun)
  ,v=1,
endfunction
if load_ref('%ans') then   pause,end,

[result,err] = int3d(X, Y, Z, f, 1, [0,100000,0.00001,0.0000001]);
if load_ref('err') then   pause,end,
if load_ref('result') then   pause,end,


xdel_run(winsid());

mclose(%U);
