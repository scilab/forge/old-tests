getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/prod_data.ref','rb');
A = [1,2;0,100];
%ans = prod(A);
if load_ref('%ans') then   pause,end,

%ans = prod(A, 'c');
if load_ref('%ans') then   pause,end,

%ans = prod(A, 'r');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
