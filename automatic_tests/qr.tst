getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/qr_data.ref','rb');
// QR factorization, generic case
// X is tall (full rank)
X = rand(5, 2);[Q,R] = qr(X);%ans = [Q' * X,R];
if load_ref('%ans') then   pause,end,

//X is fat (full rank)
X = rand(2, 3);[Q,R] = qr(X);%ans = [Q' * X,R];
if load_ref('%ans') then   pause,end,

//Column 4 of X is a linear combination of columns 1 and 2:
X = rand(8, 5);X(eye(), 4) = X(:, 1) + X(:, 2);[Q,R] = qr(X);R;
if load_ref('R') then   pause,end,
%ans = R(:, 4);
if load_ref('%ans') then   pause,end,

//X has rank 2, rows 3 to $ of R are zero:
X = rand(8, 2) * rand(2, 5);[Q,R] = qr(X);R;
if load_ref('R') then   pause,end,

//Evaluating the rank rk: column pivoting ==> rk first
//diagonal entries of R are non zero :
A = rand(5, 2) * rand(2, 5);
[Q,R,rk,E] = qr(A, 0.0000000001);
%ans = norm(Q' * A - R);
if load_ref('%ans') then   pause,end,

%ans = svd([A,Q(:, 1:rk)]);
if load_ref('%ans') then   pause,end,
//span(A) =span(Q(:,1:rk))
xdel_run(winsid());

mclose(%U);
